
use iui::UI;
use iui::controls::{WindowType, VerticalBox, Group, Button, Window, LayoutStrategy, Label, HorizontalBox, Area, AreaHandler, AreaDrawParams};
use iui::draw::{Path, FillMode, Brush, SolidBrush};
use std::f64::consts::PI;


struct HandleCanvas {}
impl AreaHandler for HandleCanvas {
    fn draw(&mut self, _area: &Area, draw_params: &AreaDrawParams) {
        let ctx = &draw_params.context;

        let path = Path::new(ctx, FillMode::Winding);
        path.add_rectangle(ctx, 0., 0., draw_params.area_width, draw_params.area_height);
        path.end(ctx);

        let brush = Brush::Solid(SolidBrush {
            r: 1.,
            g: 1.,
            b: 1.,
            a: 0.9,
        });


        draw_params.context.fill(
            &path,
            &brush,
        );

        let path = Path::new(ctx, FillMode::Winding);
        for i in 0..1500 {
            let x = i as f64 / 1500.;
            let y = ((x * PI * 5.).sin() + 1.) / 2.;
            path.add_rectangle(
                ctx,
                x * draw_params.area_width,
                y * draw_params.area_height,
                draw_params.area_width / 100.,
                5.,
            );
        }
        path.end(ctx);

        let brush = Brush::Solid(SolidBrush {
            r: 0.,
            g: 0.,
            b: 0.,
            a: 1.,
        });

        draw_params.context.fill(&path, &brush);
    }
}


fn main() {
    let ui = UI::init().unwrap();
    let mut win = Window::new(&ui, "Grapherval", 500, 400, WindowType::NoMenubar);

    let mut ui_frame = HorizontalBox::new(&ui);
    ui_frame.set_padded(&ui, true);

    let mut settings_panel = VerticalBox::new(&ui);
    settings_panel.set_padded(&ui, true);

    let mut settings_group_box = VerticalBox::new(&ui);
    let mut settings_group = Group::new(&ui, "Settings");


    let mut actions_group_box = VerticalBox::new(&ui);
    let mut actions_group = Group::new(&ui, "Actions");

    let mut quit_button = Button::new(&ui, "Quit");
    quit_button.on_clicked(&ui, {
        let ui = ui.clone();
        move |_| {
            println!("Bye!");
            ui.quit();
        }
    });

    // Create a new label. Note that labels don't auto-wrap!
    let mut label_text = String::new();
    label_text.push_str("Hey!");
    let label = Label::new(&ui, &label_text);

    settings_group_box.append(&ui, label, LayoutStrategy::Compact);
    settings_group.set_child(&ui, settings_group_box);
    actions_group_box.append(&ui, quit_button, LayoutStrategy::Compact);
    actions_group.set_child(&ui, actions_group_box);
    settings_panel.append(&ui, settings_group, LayoutStrategy::Compact);
    settings_panel.append(&ui, actions_group, LayoutStrategy::Compact);


    let mut draw_frame = HorizontalBox::new(&ui);
    draw_frame.set_padded(&ui, true);

    let area = Area::new(&ui, Box::new(HandleCanvas {}));

    let mut draw_group_box = VerticalBox::new(&ui);
    draw_group_box.append(&ui, area, LayoutStrategy::Stretchy);
    let mut draw_group = Group::new(&ui, "Preview");
    draw_group.set_child(&ui, draw_group_box);
    draw_frame.append(&ui, draw_group, LayoutStrategy::Stretchy);

    ui_frame.append(&ui, settings_panel, LayoutStrategy::Stretchy);
    ui_frame.append(&ui, draw_frame, LayoutStrategy::Stretchy);


    win.set_child(&ui, ui_frame);

    win.show(&ui);
    // Run the application
    ui.main();
}
