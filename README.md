# Grapherval

### Currently Work in progress! Might not be working (yet!)

Create discontinous graphs!

### What is it useful for?

Have you ever wanted to create a precise electronic transmission graph based on (co)sinusoids, or any other kind of mathematical formulas?
Well, you already faced that issue: If you want to change the shape of the graph depending on the X axis, your only solution would be using
a calc software, which isn't as precise as a curve! *Here's where Grapherval is interesting!*
